#include <string.h>
#include <ctype.h>
#include "mts.h"

// VT100 escape sequences
#define DEL     (127)
#define BSPACE  ('\b')
#define CR      ('\r')
#define LF      ('\n')
#define CLR_SCR ("\e[2J\e[H")
#define LEFT    ("\e[D")
#define RIGHT   ("\e[C")
#define UP      ("\e[A")
#define DOWN    ("\e[B")
#define CTRL_C  (3)

    typedef struct
{
        uint8_t cmd[MTS_HISTORY_LEN][MTS_MAX_CMD_LEN];
        size_t cmd_len[MTS_HISTORY_LEN];
        size_t ins_idx;
        size_t get_idx;
}mts_history;

typedef struct
{
        uint8_t *cmd_line;
        size_t cmd_line_size;

        size_t idx; // The cursor index in cmd_line.
        size_t num; // Number of characters in cmd_line.
        uint32_t esc; // Escape sequence state

        mts_history hist;
} mts_data;

static mts_data hdl;

static void mts_delete_char()
{
        if (hdl.idx == 0)
                return;

        if (hdl.idx == hdl.num)
        {
                hdl.idx--;
                hdl.num--;
                MTS_PUTS("\b \b");
        }
        else
        {
                MTS_PUTCHAR('\b');
                for (size_t i = hdl.idx-1; i < hdl.num; i++)
                {
                        hdl.cmd_line[i] = hdl.cmd_line[i+1];
                        MTS_PUTCHAR(hdl.cmd_line[i]);
                }
                MTS_PUTCHAR(' ');

                for (size_t i = hdl.idx - 1; i < hdl.num; i++)
                {
                        MTS_PUTS(LEFT);
                }

                hdl.idx--;
                hdl.num--;
        }
}

static void mts_insert_char(char ch)
{
        if (hdl.num >= hdl.cmd_line_size)
                return;

        if (!isprint(ch))
                return;

        if (hdl.idx == hdl.num)
        {
                hdl.cmd_line[hdl.idx] = ch;
                hdl.idx++;
                hdl.num++;
                MTS_PUTCHAR(ch);
        }
        else
        {
                for (size_t i = hdl.num + 1; i > hdl.idx; i--)
                {
                        hdl.cmd_line[i] = hdl.cmd_line[i - 1];
                }
                hdl.cmd_line[hdl.idx] = ch;

                hdl.idx++;
                hdl.num++;

                for (size_t i = hdl.idx - 1; i < hdl.num; i++)
                {
                        MTS_PUTCHAR(hdl.cmd_line[i]);
                }

                for (size_t i = hdl.idx; i < hdl.num; i++)
                {
                        MTS_PUTS(LEFT);
                }
        }
}

static void mts_replace_line(const uint8_t *line, size_t line_len)
{
        size_t length;
        size_t diff;

        if (line_len > hdl.num)
        {
                length = line_len;
                diff = 0;
        }
        else
        {
                length = hdl.num;
                diff = hdl.num - line_len;
        }

        for (size_t i = 0; i < hdl.idx; i++)
                MTS_PUTS(LEFT);

        hdl.num = hdl.idx = line_len;

        for (size_t i = 0; i < length; i++)
        {
                hdl.cmd_line[i] = line[i];
                MTS_PUTCHAR(line[i]);
        }

        for (size_t i = 0; i < diff; i++)
                MTS_PUTCHAR(' ');

        for (size_t i = 0; i < diff; i++)
                MTS_PUTS(LEFT);
}

void mts_insert_history(const uint8_t *cmd, size_t cmd_len)
{
        size_t prev = (hdl.hist.ins_idx - 1) % MTS_HISTORY_LEN;

        // Only insert if the previous entry and this cmd is not the same
        if (cmd_len != hdl.hist.cmd_len[prev] ||
                memcmp(hdl.hist.cmd[prev], cmd, cmd_len) != 0)
        {
                memcpy(hdl.hist.cmd[hdl.hist.ins_idx], cmd, cmd_len);
                hdl.hist.cmd_len[hdl.hist.ins_idx] = cmd_len;

                hdl.hist.ins_idx = (hdl.hist.ins_idx + 1) % MTS_HISTORY_LEN;

                if (hdl.hist.get_idx == hdl.hist.ins_idx)
                        hdl.hist.get_idx = (hdl.hist.get_idx + 1) % MTS_HISTORY_LEN;
        }

        hdl.hist.get_idx = hdl.hist.ins_idx;
}

const uint8_t *mts_history_up(size_t *cmd_len)
{
        size_t temp = (hdl.hist.get_idx - 1) % MTS_HISTORY_LEN;

        if (temp == hdl.hist.ins_idx || !hdl.hist.cmd_len[temp])
        {
                *cmd_len = 0;
                return 0;
        }

        hdl.hist.get_idx = temp;

        const uint8_t *cmd = hdl.hist.cmd[hdl.hist.get_idx];
        *cmd_len = hdl.hist.cmd_len[hdl.hist.get_idx] ;

        return cmd;
}

const uint8_t *mts_history_down(size_t *cmd_len)
{
        size_t temp = (hdl.hist.get_idx + 1) % MTS_HISTORY_LEN;

        if (temp == hdl.hist.ins_idx ||
            !hdl.hist.cmd[temp] || !hdl.hist.cmd_len)
        {
                *cmd_len = 0;
                return 0;
        }

        hdl.hist.get_idx = temp;

        const uint8_t *cmd = hdl.hist.cmd[hdl.hist.get_idx];
        *cmd_len = hdl.hist.cmd_len[hdl.hist.get_idx];

        return cmd;
}

bool mts_init(uint8_t *cmd_line, size_t cmd_line_size)
{
        if (!cmd_line)
                return false;

        if (cmd_line_size < MTS_MAX_CMD_LEN)
                return false;

        hdl.cmd_line = cmd_line;
        hdl.cmd_line_size = cmd_line_size;

        hdl.num = hdl.idx = 0U;
        hdl.esc = 0U;

        hdl.hist.ins_idx = 0;
        hdl.hist.get_idx = 0;
        for (size_t i = 0U; i < MTS_HISTORY_LEN; i++)
                hdl.hist.cmd_len[i] = 0;

        mts_insert_history(NULL, 0);

        return true;
}

/* Returns >0 if a complete command line was received and written to cmd_line. */
size_t mts_process(uint8_t ch)
{
        if (ch == '\e' && hdl.esc == 0)
        {
                hdl.esc = 1;
                return 0;
        }
        else if (hdl.esc == 1)
        {
                if (ch == '[')
                {
                        hdl.esc = 2;
                }
                else
                {
                        // invalid escape sequence
                        hdl.esc = 0;
                }
                return 0;
        }
        else if (hdl.esc == 2)
        {
                hdl.esc = 0;

                // Left arrow
                if (ch == 'D')
                {
                        if (hdl.idx)
                        {
                                hdl.idx--;
                                MTS_PUTS(LEFT);
                        }
                }
                // Right arrow
                if (ch == 'C')
                {
                        if (hdl.idx < hdl.num)
                        {
                                hdl.idx++;
                                MTS_PUTS(RIGHT);
                        }
                }
                // Up arrow
                if (ch == 'A')
                {
                        const uint8_t *cmd;
                        size_t cmd_len;
                        cmd = mts_history_up(&cmd_len);
                        if (cmd && cmd_len)
                                mts_replace_line(cmd, cmd_len);
                }
                if (ch == 'B')
                {
                        const uint8_t *cmd;
                        size_t cmd_len;
                        cmd = mts_history_down(&cmd_len);
                        if (cmd && cmd_len)
                                mts_replace_line(cmd, cmd_len);
                }
                return 0;
        }

        if (ch == CR || ch == LF)
        {
                size_t num = hdl.num;
                MTS_PUTS(MTS_NL);
                hdl.num = hdl.idx = 0;
                mts_insert_history(hdl.cmd_line, num);
                return num;
        }

        if (ch == BSPACE || ch == DEL)
        {
                mts_delete_char();
                return 0;
        }

        if (ch == CTRL_C)
        {
                MTS_PUTS(MTS_NL MTS_PROMPT); // ToDo: Only resets command line!
                hdl.num = hdl.idx = 0;
                hdl.hist.get_idx = hdl.hist.ins_idx;
                return 0;
        }

        mts_insert_char(ch);

        return 0;
}

void mts_clear_screen()
{
        MTS_PUTS(CLR_SCR);
}
