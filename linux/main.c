#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "tty.h"
#include "mts.h"

int main(int argc, char *argv[])
{
        uint8_t cmd_buf[MTS_MAX_CMD_LEN];

        if (argc < 2)
        {
                printf("Usage: %s <port>\n", argv[0]);
                return -1;
        }

        if (tty_open(argv[1], B115200, 0) != 0)
        {
                printf("Error while open tty %s\n", argv[1]);
                return -1;
        }

        if (!mts_init(cmd_buf, MTS_MAX_CMD_LEN))
        {
                printf("Error while initializing MCU Terminal Server\n");
                return -1;
        }

        mts_clear_screen();
        MTS_PUTS("Welcome to MCU Terminal Server" MTS_NL MTS_NL MTS_PROMPT);

        for(;;)
        {
                int c = tty_getc();
                printf("RX: %d (%c)\n", c, (char)c);

                size_t cmd_len = mts_process(c);
                if (cmd_len)
                {
                        printf("Execute command\n");
                        MTS_PUTS("Executing: ");
                        for (int i = 0; i < cmd_len; i++)
                                if (isprint(cmd_buf[i]))
                                        MTS_PUTCHAR(cmd_buf[i]);
                                else
                                        MTS_PUTCHAR('.');
                        MTS_PUTS(MTS_NL MTS_PROMPT);
                }
        }

        return 0;
}