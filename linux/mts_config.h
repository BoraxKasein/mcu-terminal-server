#include "tty.h"

#define MTS_NL "\r\n"             // What will be send as end of line.
#define MTS_PROMPT "user@mts$ "   // Prompt like PS1.
#define MTS_MAX_CMD_LEN (80)      // Maximum length of a command.
#define MTS_HISTORY_LEN (16)      // Length of command history.

#define MTS_PUTCHAR(c) tty_putchar(c) // Macro to print a character.
#define MTS_PUTS(s) \
do {                                    \
        int i = 0;                      \
        while (s[i])                    \
                MTS_PUTCHAR(s[i++]);    \
} while(0)
