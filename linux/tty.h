#ifndef tty_H__
#define tty_H__

#include <termios.h>

int tty_open(const char *port, int speed, int parity);
int tty_putchar(int c);
int tty_getc();

#endif /* tty_H__ */