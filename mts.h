/* MTS stands for MCU Terminal Server. */

#ifndef mts_H__
#define mts_H__

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "mts_config.h"

bool mts_init(uint8_t *buf, size_t buf_size);
/* Reads command line, delete, curser, history. Is called prior to the command call. */
size_t mts_process(uint8_t ch);
void mts_clear_screen();

#endif /* mts_H__ */